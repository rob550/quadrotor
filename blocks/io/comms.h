/*
 * comms.h
 *
 *  Created on: Oct 5, 2014
 *      Author: jonathan
 */

#ifndef COMMS_H_
#define COMMS_H_

#include <stdint.h>
#include <stdbool.h>

#include "container.h"

typedef enum comms_channel_t
{
    CHANNEL_ALL,
    CHANNEL_KILL,
    CHANNEL_CHANNELS,        // Carries HW inputs from Blocks->Linux or autonomous cmds from Linux->Blocks
    CHANNEL_CHANNELS_OUTPUT, // Exclusively transmits HW outputs from Blocks->Linux
    CHANNEL_TELEMETRY,
    CHANNEL_LED,
    CHANNEL_CFG_USB_SN,
    CHANNEL_CFG_DATA_FREQUENCY,
    CHANNEL_CFG_CHANNELS_DEFAULT,
    CHANNEL_CFG_UART_BAUD,
    CHANNEL_DEBUG,
    CHANNEL_NUM_CHANNELS

} comms_channel_t;

#define COMMS_STATUSES\
    X(COMMS_STATUS_INVALID_ARGUMENT) \
    X(COMMS_STATUS_OFF_SYNC)         \
    X(COMMS_STATUS_WRONG_CHECKSUM)   \
    X(COMMS_STATUS_BUFFER_FULL)      \
    X(COMMS_STATUS_WAITING)          \
    X(COMMS_STATUS_IN_PROGRESS)      \
    X(COMMS_STATUS_NO_ACTION)        \
    X(COMMS_STATUS_BUFFERED)         \
    X(COMMS_STATUS_DONE)             \
    X(COMMS_STATUS_SUCCESS)          \
    X(COMMS_STATUS_NUM_STATUSES)     \

typedef enum comms_status_t
{
    #define X(a) a,
    COMMS_STATUSES
    #undef X
} comms_status_t;

typedef struct comms_t comms_t;

extern container_funcs_t *comms_cfuncs;

typedef void (*publisher_t)(container_t *data, uint16_t id);

typedef void (*subscriber_t)(void *usr, uint16_t id, comms_channel_t channel,
                             const uint8_t *msg, uint16_t len);




comms_t* comms_create(uint32_t buf_len_rx, uint32_t buf_len_tx,
                      uint32_t num_tx_orig, publisher_t publisher);

comms_status_t comms_subscribe(comms_t *comms, comms_channel_t channel,
                               subscriber_t subscriber, void *usr);

comms_status_t comms_publish(comms_t *comms,
                             uint16_t id,
                             comms_channel_t channel,
                             const uint8_t *msg,
                             uint16_t msg_len,
                             uint32_t tx_origin_num);

inline comms_status_t comms_transmit(comms_t *comms);

comms_status_t comms_handle_now(comms_t *comms, uint8_t byte);

comms_status_t comms_handle_later(comms_t *comms, uint8_t byte);

comms_status_t comms_handle(comms_t *comms);

void comms_destroy(comms_t *comms);

uint32_t comms_get_metadata(comms_t *comms, comms_status_t status);

void comms_metadata_printf(comms_t *comms);

#define COMMS_METADATA_PRINTF(X) \
    do{\
        printf(#X" metadata:\n");\
        comms_metadata_printf(X);\
    }while(0)

char* comms_status_to_str(comms_status_t status, char *buf, uint8_t len);

#endif /* COMMS_H_ */
